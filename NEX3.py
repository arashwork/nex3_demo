# Nextinput Libraries
from nextinput.gui import DataProcess, Gui ,start_app
from nextinput.devices import NIDevice
from nextinput import niutils

# third party libraries
import sys
from heapq import nlargest
from multiprocessing import Value, Array
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap,QFont
import time
import json
from argparse import ArgumentParser
import numpy as np
import math
import os
from scipy import stats, signal
from scipy.stats import linregress
import pandas as pd

logger = niutils.create_logger(__name__)

# global variable
#
version = 'v1.60'

PRESS_EVENT = 1
NEGATIVE_EVENT = -1
NO_EVENT = 0
RELEASE_EVENT = 2
DOUBLE_PRESS_EVENT = 3
class sidekey_DataProcess(DataProcess):
    def setup(self):
        # adding extra key binding functions
        self.add_cmd(Qt.Key.Key_0, self.set_btn)
        self.add_cmd(Qt.Key.Key_1, self.set_btn)
        self.add_cmd(Qt.Key.Key_2, self.set_btn)
        self.add_cmd(Qt.Key.Key_3, self.set_btn)
        self.add_cmd(Qt.Key.Key_4, self.set_btn)
        self.add_cmd(Qt.Key.Key_5, self.set_btn)
        self.add_cmd(Qt.Key.Key_6, self.set_btn)
        self.add_cmd(Qt.Key.Key_7, self.set_btn)
        self.add_cmd(Qt.Key.Key_8, self.set_btn)
        self.add_cmd(Qt.Key.Key_9, self.set_btn)

        self.actual_btn = 0
        self.btn_dic = {Qt.Key.Key_0 : 0,Qt.Key.Key_1 : 1,Qt.Key.Key_2 : 2, Qt.Key.Key_3 : 3, Qt.Key.Key_4 : 4,Qt.Key.Key_5 : 5,
                        Qt.Key.Key_6 : 6,Qt.Key.Key_7 : 7,Qt.Key.Key_8 : 8, Qt.Key.Key_9 : 9}
        
        #setup number of sensors, used for csv data later on 
        self.num_sensors = self.enum_count
  
        #setup buffers
        self.raw_buf = [[] for i in range(self.num_sensors)]
        self.median_buf = [[] for i in range(self.num_sensors)]
        self.filter_buf = [[] for i in range(self.num_sensors)]
        self.hpf_buf = [[] for i in range(self.num_sensors)]
        # self.guass_window = [0.68194075, 0.82257756, 0.93210249, 0.99221794, 0.99221794, 0.93210249,0.82257756, 0.68194075]
        self.guass_window = [0.5,0.5,1,1]

        
        # high pass filter coeffcient
        
        # high pass at 0.05wn
        self.hpf_b = [-0.93]
        self.hpf_a = [0.97,-0.97]
        self.hpf_press_thr = 45
        
        # # high pass at 0.08wn
        # self.hpf_b = [-0.77]
        # self.hpf_a = [0.88,-0.88]
        # self.hpf_press_thr = 20

        #setup the sensor data and state machine
        self.filter = [0 for i in range(self.num_sensors)]
        self.baseline = [0 for i in range(self.num_sensors)]
        self.fil_adc = [0 for i in range(self.num_sensors)]
        self.scalar = [1 for i in range(self.num_sensors)]
        self.tunned_adc = [0 for i in range(self.num_sensors)]

        self.hpf = [0 for i in range(self.num_sensors)]
        self.hpf_slope = [0 for i in range(self.num_sensors)]
        self.ratio_hpf = [0 for i in range(self.num_sensors)]
        self.forcebl_flag = [0 for i in range(self.num_sensors)]
        
        #setup state machines for device 
        self.global_event = 0 # used to specify device state, 0 no event, 1 is press event, 2 is release event, -1 negative event, -2 release of negative event
        self.btn_event = 0 # specifies which button has an touch event on it, 0 for no button, 1 for button one, 2 for button 2, 3 for button 3, if its negative, it means those buttons went negative first 
        self.btn = 0 # sets the button that is being pressed and is a valid press

        
        self.stuck_limit = 5000
        self.stuck_ctr = 0

        self.magnitude  = 0
        self.hpf_magnitude = 0
        self.slope_mag = 0
        self.settle_ctr = 0
        self.actual_btn = 0

        # high pass value for detecting events 
        self.vm = 0 
        self.pw = 0
        self.vp = 0

        self.vm_slope = 0 
        self.pw_slope = 0
        self.vp_slope = 0

        # variables of button confidences 
        self.cf1 = 0
        self.cf2 = 0
        self.cf3 = 0

        self.use_csv = False
        if(not self.read_csv):
            # Hardware registers
            for dev in self.devs:
                dev.set_reg('TEMPWAIT', 1)
                dev.set_reg('WAIT', 1)
                dev.set_reg('EN', 1)
                dev.set_reg('AUTOPRELDADJ', 1)
                dev.set_reg('ADCRAW', 0)

                # guage design
                dev.set_reg('INAGAIN', 6)
                dev.set_reg('PRECHARGE', 5)
                dev.set_reg('CALPERIOD',    4)
                dev.set_reg('RISEBLWGT',    1)
                dev.set_reg('FALLBLWGT',   1)
                dev.set_reg('FORCEBL', 1)
                time.sleep(0.5)

            time.sleep(0.1)

            for dev in self.devs:
                dev.optimize_preldadj(target=0.0)
                dev.set_reg('FORCEBL', 1)
        else:
            self.row = 0
            self.raw = [0] * self.num_sensors
            self.bl = [0] * self.num_sensors
            self.adc = [0] * self.num_sensors
            self.toggle_log()

        return True


    def gen_log_header(self):
        return ','.join([ 'sampple' if self.read_csv else super().gen_log_header(),
                         ','.join([f'r{d+1},filtered{d + 1},baseline{d + 1},adc{d + 1},hpf{d + 1},hpf_slope{d + 1},hpfR{d + 1}' for d in range(self.num_sensors)]),\
                         'magnitude,hpf_magnitude,slope_mag,btn,actual_btn,global_event,btn_event,vm,pw,vp,vm_slope,pw_slope,vp_slope,cf1,cf2,cf3'
                         ])

    def gen_log_data(self):
        return ','.join([f'{self.row}' if self.read_csv else  super().gen_log_data(),
                         ','
                         .join([f'{self.raw[d]},{self.filter[d]},{self.baseline[d]},{self.tunned_adc[d]},{self.hpf[d]},{self.hpf_slope[d]},{self.ratio_hpf[d]}' for d in range(self.num_sensors)])
                         ,f'{self.magnitude},{self.hpf_magnitude},{self.slope_mag},\
                         {self.btn},{self.actual_btn},{self.global_event},{self.btn_event},\
                         {self.vm},{self.pw},{self.vp},\
                         {self.vm_slope},{self.pw_slope},{self.vp_slope},\
                         {self.cf1},{self.cf2},{self.cf3}'\
                         ])


    def force_baseline(self):
        for d in range(self.num_sensors):
                self.baseline[d] = self.filter[d]

    def flush_buffer(self):
        self.raw_buf = [[] for i in range(len(self.devs))]
        self.median_buf = [[] for i in range(len(self.devs))]
        self.filter_buf = [[] for i in range(len(self.devs))]

    def set_btn(self):
        self.actual_btn = self.btn_dic[self.cmd.value]

    def toggle_log(self):
        if self._logfd:
            logger.info('Stop log')
            self._logfd.close()
            self._logfd = None
        else:
            self.tl = time.perf_counter()
            try:
                if(self.read_csv == True):
                    self._logfd = open('log_c.csv', 'w')
                else:
                    self._logfd = open('log.csv', 'w')
            except PermissionError:
                logger.warning('Could not start log')
            else:
                logger.info('Start log')
                self._logfd.write(self.gen_log_header())
                self._logfd.write('\n')
                self._reg_gen = NIDevice.gen_reg_vals()

    def enum(self):
        logger.debug(f'Enumerating: extras = {self.enum_extras}, count = {self.enum_count}')

        self.devs = NIDevice.enum(i2c_extras=self.enum_extras)

        if self.enum_count > 0:
            if len(self.devs) != self.enum_count:
                logger.warning(f'Enum count expected {self.enum_count}; got {len(self.devs)}')
                if(self.filename != None):
                    df = pd.read_csv(self.filename)
                    col = [f'r{s+1}' for s in range(self.enum_count)]
                    self.data = np.array(df[col]).astype(int) 
                    self.read_csv = True
                    self.ready.value = 1

                else:
                    self.ready.value = -1
        else:
            if len(self.devs) == 0:
                logger.warning('Enum count got 0')
                self.ready.value = -1

        if self.enum_extras:
            self.haptics = NIDevice.haptics

        return self.ready.value >= 0

    def relu(self,val):
        return  max(0,val)

    def relu_abs(self,val,band):
        return  max(0,abs(val)-band)

    def poll_data(self):
        if(self.read_csv == True):
            try:
                for s in range(self.num_sensors):
                    self.raw[s] = self.data[self.row][s]
                self.row +=1
            except IndexError:
                print("end of file")
                self.toggle_log()
                sys.exit()
        else:
            adc = self.devs[0].get_reg('ADCOUT')
            bl = self.devs[0].get_reg('BASELINE')
            temp = [0 for d in range(self.num_sensors)]
            for d in range(self.num_sensors):
                self.adc[d] =  self.devs[d].get_reg('ADCOUT')
                self.bl[d] = self.devs[d].get_reg('BASELINE')
                temp[d] = self.adc[d] + self.bl[d]

            self.raw[0] = temp[2]
            self.raw[1] = temp[3]
            self.raw[2] = temp[0]
            self.raw[3] = temp[1]
            self.raw[4] = temp[4]

        # filter data 
        self.filter_data()
 
    def filter_data(self):
        for d in range(self.num_sensors):
            #fill up raw buffer
            self.raw_buf[d].append(self.raw[d])
            fil = 0
            
            if(len(self.raw_buf[d]) > len(self.guass_window)):
                self.raw_buf[d].pop(0)
                self.filter_buf[d].pop(0) # doint need the filter to be so deep but lets keep for now
                self.median_buf[d].pop(0)
                self.hpf_buf[d].pop(0)

                med = np.median(self.raw_buf[d][-3:])
                self.median_buf[d].append(med)
                
                for ridx in range(len(self.guass_window)):
                    fil += self.median_buf[d][-ridx]*self.guass_window[ridx] # need to doble check conversion
                
                fil = fil/np.sum(self.guass_window)
                self.hpf[d] =  self.hpf_a[0]*fil + self.hpf_a[1]*self.filter[d] - self.hpf_b[0]*self.hpf[d]
                self.filter[d] = fil 
                self.filter_buf[d].append(self.filter[d])

                slope, intercept, r_value, p_value, std_err = linregress([i+1 for i in range(len(self.hpf_buf[d]))], self.hpf_buf[d])
                self.hpf_slope[d] = slope

            else:
                fil = np.mean(self.raw_buf[d])
                
                if(len(self.filter_buf[d]) < 2):
                    self.hpf[d] = 0
                    self.baseline[d] = fil
                else:
                    self.hpf[d] =  self.hpf_a[0]*fil + self.hpf_a[1]*self.filter[d] - self.hpf_b[0]*self.hpf[d]

                self.filter[d] = fil
                self.filter_buf[d].append(fil)
                self.median_buf[d].append(fil)
            
            self.hpf_buf[d].append(self.hpf[d])

    def detect_event(self):
        prev_event = self.global_event 

        self.hpf_magnitude = 0
        self.slope_mag = 0
        for d in range(self.num_sensors):
            self.hpf_magnitude += abs(self.hpf[d]*self.scalar[d]) 
            self.slope_mag += abs(self.hpf_slope[d])

        
        self.vm = self.hpf[0] + self.hpf[1]
        self.pw = self.hpf[2] + (self.hpf[1] + self.hpf[3])/2
        self.vp = self.hpf[3] + self.hpf[4]
        button_hpf = [self.vm,self.pw,self.vp]
        button_hpf_abs = [abs(self.vm), abs(self.pw), abs(self.vp)]

        self.vm_slope = self.hpf_slope[0] + self.hpf_slope[1]
        self.pw_slope = self.hpf_slope[2] + (self.hpf_slope[1] + self.hpf_slope[3])/2
        self.vp_slope = self.hpf_slope[3] + self.hpf_slope[4]
        button_slopes = [self.vm_slope,self.pw_slope,self.vp_slope]
        

        if((self.global_event == PRESS_EVENT or self.global_event == DOUBLE_PRESS_EVENT ) and np.max(button_slopes) > 0 ):
            for d in range(self.num_sensors):
                self.ratio_hpf[d] = self.hpf[d]*self.scalar[d]/self.hpf_magnitude

            # button one function
            self.cf1 = 1
            self.cf1 -= 3*self.relu(0.45-self.ratio_hpf[0])
            self.cf1 -= 5*self.relu(self.ratio_hpf[1]- 0.15 - self.ratio_hpf[0])
            self.cf1 -= 2*self.relu_abs(self.ratio_hpf[2],0.1)
            self.cf1 -= 2*self.relu_abs(self.ratio_hpf[3],0.1)
            self.cf1 -= 2*self.relu(self.ratio_hpf[4])
            self.cf1 -= 3*self.relu(-1*self.ratio_hpf[1])

            #button two function 
            self.cf2 = 1
            self.cf2 -= 5*self.relu(0.25-self.ratio_hpf[2])
            self.cf2 -= 5*self.relu(self.ratio_hpf[1] - self.ratio_hpf[2]-0.1)
            self.cf2 -= 5*self.relu(self.ratio_hpf[3] - self.ratio_hpf[2]-0.1)
            self.cf2 -= 2*self.relu_abs(self.ratio_hpf[0] - self.ratio_hpf[4],0.25)
            self.cf2 -= 5*self.relu(self.ratio_hpf[4] - 0.05)
            self.cf2 -= 5*self.relu(self.ratio_hpf[0] - 0.05)

            #button three function 
            self.cf3 = 1
            self.cf3 -= 5*self.relu(0.3-self.ratio_hpf[4])
            self.cf3 -= 3*self.relu(self.ratio_hpf[3] - self.ratio_hpf[4] - 0.3)
            self.cf3 -= 2*self.relu_abs(self.ratio_hpf[0] - self.ratio_hpf[1],0.2)
            self.cf3 -= 3*self.relu(self.ratio_hpf[0] - 0.1)
            self.cf3 -= 3*self.relu(self.ratio_hpf[1] - 0.1)


        else:
            self.ratio_hpf  = [0 for s in range(self.num_sensors)]

            self.cf1 = 0
            self.cf2 = 0
            self.cf3 = 0


        button_confs = [self.cf1, self.cf2, self.cf3]

        ### everything above is good now just need to do edge detection
        if(self.global_event == NO_EVENT and np.max(button_hpf_abs) > self.hpf_press_thr):
            
            if(np.argmax(button_hpf_abs) ==  np.argmax(button_hpf)):
                self.global_event = PRESS_EVENT
            else:
                self.global_event = NEGATIVE_EVENT
        
        elif(self.global_event == PRESS_EVENT and np.max(button_hpf) > self.hpf_press_thr and button_slopes[np.argmax(button_hpf)] > 0):
            self.btn_event = np.argmax(button_hpf) + 1 # set the button to the highest button possible 
            
            if(self.btn_event > 0 and button_confs[self.btn_event -1] > 0.9):
                self.btn = self.btn_event 
            else:
                self.btn = 0 #TODO: need to comeback and reconsider how i am treating this and potentially remove it 

        elif(self.global_event == DOUBLE_PRESS_EVENT and self.btn_event > 0 and button_hpf[self.btn_event-1] > self.hpf_press_thr*0.3 and button_slopes[self.btn_event-1] > 0 ):
            
            if(button_confs[self.btn_event -1] > 0.9):
                self.btn = self.btn_event
            else:
                self.btn = 0

        elif(self.global_event == PRESS_EVENT and self.btn != 0 and button_hpf[self.btn - 1] < -1*self.hpf_press_thr*0.6 ): #valid button was pressed and is now being released
            self.global_event = RELEASE_EVENT
            self.btn = 0 # this sets the GUI button to zero, but we are still tracking the button using the btn_event flag
        
        elif(self.global_event == DOUBLE_PRESS_EVENT and self.btn != 0 and button_hpf[self.btn - 1] < -1*self.hpf_press_thr*0.3 ): #valid button was pressed and is now being released
            self.global_event = RELEASE_EVENT
            self.btn = 0 # this sets the GUI button to zero, but we are still tracking the button using the btn_event flag

        elif(self.global_event == PRESS_EVENT and self.btn == 0 and (np.argmax(button_hpf_abs) !=  np.argmax(button_hpf)) and np.min(button_hpf) < -1*self.hpf_press_thr*0.8):
            self.global_event = RELEASE_EVENT

        elif(self.global_event == DOUBLE_PRESS_EVENT and self.btn == 0 and (np.argmax(button_hpf_abs) !=  np.argmax(button_hpf)) and np.min(button_hpf) < -1*self.hpf_press_thr*0.3):
            self.global_event = RELEASE_EVENT

        elif(self.global_event == NEGATIVE_EVENT and np.min(button_hpf) < -1*self.hpf_press_thr and button_slopes[np.argmin(button_hpf)] < 0):
            self.btn_event = -1*(np.argmax(button_hpf) + 1) # button that caused the negative event

        elif(self.global_event == NEGATIVE_EVENT and self.btn_event < 0 and button_hpf[-1*self.btn_event - 1] > self.hpf_press_thr*0.8): # negative signal popped back up   
            self.global_event = RELEASE_EVENT

        elif(self.global_event == RELEASE_EVENT and np.max(button_hpf_abs) < self.hpf_press_thr*0.5 and np.max(button_slopes) < 5 and np.min(button_slopes) > -5):
            self.global_event = NO_EVENT # now that buttons have all settled, we can clear the event  
            self.btn_event = 0

        elif(self.global_event == RELEASE_EVENT and self.btn_event > 0 and button_hpf[self.btn_event-1] > self.hpf_press_thr*0.3 ): #and button_slopes[self.btn_event - 1] > 0
            self.global_event = DOUBLE_PRESS_EVENT # this will allow any button, may need to limit it to just the one that was released
       
        if(self.btn != 0):
            self.stuck_ctr += 1
            if(self.stuck_ctr > self.stuck_limit):
                self.btn = 0 # implement stuck recovery here
                self.global_event = RELEASE_EVENT
        
        elif(self.btn == 0 and (self.global_event == PRESS_EVENT or self.global_event == NEGATIVE_EVENT or self.global_event == DOUBLE_PRESS_EVENT)):
            self.stuck_ctr += 1
            if(self.stuck_ctr > (self.stuck_limit/10) ):
                self.btn = 0 # implement stuck recovery here
                self.global_event = RELEASE_EVENT
        else:
            self.stuck_ctr = 0
        
        if(self.global_event != NO_EVENT):
            for s in range(self.num_sensors):
                self.forcebl_flag[s] = False
        
        elif(self.global_event == NO_EVENT and prev_event != NO_EVENT and self.btn == 0):
            for s in range(self.num_sensors):
                self.forcebl_flag[s] = True

    def update(self):
        if(self.global_event == NO_EVENT):
            for d in range(self.num_sensors):
                if(self.forcebl_flag[d] == 1 and abs(self.hpf_magnitude) < 5 and abs(self.hpf_slope[d]) <=1):
                    self.baseline[d] = self.filter[d]
                    self.forcebl_flag[d] = 0
                
                elif(self.hpf_magnitude < 20):
                    w = 8 + 1*self.hpf[d]**2 + 1*abs(self.fil_adc[d]) + self.hpf_magnitude
                    self.baseline[d] = (w*self.baseline[d] + self.filter[d])/(w+1)

        self.magnitude = 0
        for d in range(self.num_sensors):
            self.fil_adc[d] = self.filter[d] - self.baseline[d]
            self.tunned_adc[d] = self.fil_adc[d]*self.scalar[d]  

            self.magnitude += abs(self.tunned_adc[d])
             
    def process_data(self):

        for d in range(self.num_sensors):
            self.data_string[d] = int(self.fil_adc[d])
        # Choose image to display
        self.btn_img.value = self.btn

class sidekey_gui(Gui):

    def setup_process(self):

        self.debug_mode = False

        self.enum_extras = True
        self.enum_count = 5
        self.btn_img  = Value('i', 4)
        self.dev_id = Value('i', 0)
        self.data_string = Array('i', [0]*(self.enum_count))
        self.recovery = Value('i', 0)

        self.process_kwargs.update({'btn_img': self.btn_img, 'data_string':self.data_string, 'recovery' : self.recovery})
        self.process = sidekey_DataProcess


    def keyPressEvent(self, event):
        if int(event.modifiers()) == 0:
            if(event.key() == Qt.Key.Key_D ):
                self.switch_mode()
            if event.key() in self.cmds:
                self.cmds[event.key()]()
            else:
                self.cmd.value = event.key()

    def initUI(self):
        # Fill background
        self.fillBackground(Qt.white)

        # Load images
        self.btn_img_lst = [
            QPixmap('phone_nobtn.png'),
            QPixmap('phone_vminus.png'),
            QPixmap('phone_pwr.png'),
            QPixmap('phone_vplus.png'),
            QPixmap('phone_recovery.png')
        ]

        # mode name listes


        # Rescale images
        for i in range(len(self.btn_img_lst)):
            w = min(self.btn_img_lst[i].width(), self.width - 100)
            h = min(self.btn_img_lst[i].height(), self.height - 100)
            self.btn_img_lst[i] = self.btn_img_lst[i].scaled(w, h, Qt.KeepAspectRatio, transformMode=Qt.SmoothTransformation)

        # Create and center image widget
        self.btn_pic = QLabel(self)
        self.btn_pic.setPixmap(self.btn_img_lst[self.btn_img.value])
        self.btn_pic.move((self.width - self.btn_img_lst[self.btn_img.value].width()) / 2,
                          (self.height - self.btn_img_lst[self.btn_img.value].height()) / 2)


        # data label
        self.data_label = QLabel(self)

        self.data_label.move((self.width)/2, 0)
        # self.data_label.setFixedWidth(self.width)

        data = ','.join([f'{0}'for d in range(self.enum_count)])
        self.data_label.setText(data)
        self.data_label.setFont(QFont('Helvatica', 20))
        self.data_label.setStyleSheet("""QLabel {
                                        color: #000000;}""")
        self.data_label.setAlignment(Qt.AlignCenter)
        self.data_label.hide()

        self.show()

    def switch_mode(self):
        if(self.debug_mode):
            self.debug_mode = False
            self.data_label.hide()
        else:
            self.debug_mode = True
            self.data_label.show()

    def updateUI(self):
        # Change picture
        self.btn_pic.setPixmap(self.btn_img_lst[self.btn_img.value])
        

        if(self.debug_mode):
            data = ','.join([f'{self.data_string[d]}' for d in range(self.enum_count)])
            self.data_label.setText(data)
            self.data_label.adjustSize() 
    


        if self.ready.value < 0:
            self.close()


if __name__ == '__main__':
    start_app(sidekey_gui, title='NEX Demo')